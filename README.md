## Pre-requirement

```
> yarn install
> cp .env.example .env
// modify .env
```

## Available scripts

```
> yarn start <-- local dev server
> yarn build <-- build folder will be generated
```
