const API_URL = process.env.REACT_APP_API_URL;

export const API = {
  regScheduleFind: API_URL + "/regSchedule/find",
  getViewInfo: API_URL + "/hixClinicRoom/getViewInfo",
};
