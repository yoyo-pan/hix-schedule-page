import { Box } from "@mui/system";
import { useEffect, useState } from "react";
import { API } from "../../util";
import dayjs from "dayjs";
import { Schedule } from "../../types/schedule";
import ScheduleTable from "../../packages/ScheduleTable";
import { ViewInfo } from "../../types/viewInfo";
import CurrentViewNumber from "../../packages/CurrentViewNumber";

export default function ClinicSchedulePage() {
  const [schedules, setSchedules] = useState<Schedule[]>([]);
  const [viewInfo, setViewInfo] = useState<ViewInfo[]>([]);

  useEffect(() => {
    window
      .fetch(API.regScheduleFind, {
        body: JSON.stringify({
          dateFrom: dayjs().format("YYYY-MM-DD"),
          dateTo: dayjs().add(6, "day").format("YYYY-MM-DD"),
          flagType: "1",
        }),
        headers: {
          "content-type": "application/json",
        },
        method: "POST",
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.code !== 0) {
          window.alert("無法取得看診班表，請稍後再試");
          return;
        }
        setSchedules(data.result);
      });

    window
      .fetch(API.getViewInfo, {
        body: JSON.stringify({ clinicRoomIds: ["01", "02"] }),
        headers: {
          "content-type": "application/json",
        },
        method: "POST",
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.code !== 0) {
          window.alert("無法取得當前看診號，請稍後再試");
          return;
        }
        setViewInfo(data.result);
      });
  }, []);

  return (
    <Box
      sx={{
        height: "100vh",
        background: "#ccdbfd",
        paddingTop: "48px",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <CurrentViewNumber viewInfo={viewInfo} />
      {schedules.length > 0 && <ScheduleTable schedules={schedules} />}
      <Box
        component="footer"
        sx={{
          fontSize: "18px",
          color: "red",
          mt: "24px",
          fontWeight: "bold",
        }}
      >
        本表每週更新，實際門診表會依狀況修訂，正確資訊請於就診當日向診所查詢。
      </Box>
    </Box>
  );
}
