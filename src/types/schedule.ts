export type ShiftId = "AM" | "PM" | "EVENING";

export interface Schedule {
  id: number;
  doctorName: string;
  hixClinicRoomId: string;
  hixClinicRoomName: string;
  schDate: string; // "2022-09-29T00:00:00.000Z"
  refShiftId: ShiftId;
  refShiftName: string;
}
