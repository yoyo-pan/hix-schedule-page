export interface ViewInfo {
  id: string;
  name: string;
  viewInfo: {
    dateTime?: string; // "2022-02-17 14:20:15",
    no?: number;
  };
}
