import { CssBaseline } from "@mui/material";
import ClinicSchedulePage from "./pages/ClinicSchedule";

function App() {
  return (
    <main>
      <CssBaseline />
      <ClinicSchedulePage />
    </main>
  );
}

export default App;
