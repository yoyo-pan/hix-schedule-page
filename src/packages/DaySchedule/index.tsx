import { Box } from "@mui/system";
import { Dayjs } from "dayjs";

interface CurrentSchedule {
  doctorName: string;
  shiftTime: string;
}

interface Props {
  day: string;
  date: Dayjs;
  amSchedule: CurrentSchedule;
  pmSchedule: CurrentSchedule;
  eveningSchedule: CurrentSchedule;
}

export function DayScheduleHeader(props: {
  amRoomName: string;
  pmRoomName: string;
  eveningRoomName: string;
}) {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        textAlign: "center",
        width: "100px",
      }}
    >
      <div
        style={{
          background: "#FFFF99",
          padding: "2px",
          border: "solid 1px black",
        }}
      >
        診別
      </div>
      <div
        style={{
          background: "pink",
          height: "30px",
          border: "solid 1px black",
        }}
      ></div>
      <section
        style={{
          background: "#E1FFDA",
          lineHeight: "76px",
          height: "76px",
          border: "solid 1px black",
        }}
      >
        上午{props.amRoomName}
      </section>
      <section
        style={{
          background: "#FFCCFF",
          lineHeight: "76px",
          height: "76px",
          border: "solid 1px black",
        }}
      >
        下午{props.pmRoomName}
      </section>
      <section
        style={{
          background: "#CCCCFF",
          lineHeight: "76px",
          height: "76px",
          border: "solid 1px black",
        }}
      >
        夜間{props.eveningRoomName}
      </section>
    </Box>
  );
}

export default function DaySchedule({
  day,
  date,
  amSchedule,
  pmSchedule,
  eveningSchedule,
}: Props) {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        textAlign: "center",
        fontSize: "16px",
      }}
    >
      <div
        style={{
          background: "#FFFF99",
          padding: "2px",
          border: "solid 1px black",
        }}
      >
        {day}
      </div>
      <div
        style={{
          background: "pink",
          padding: "2px",
          border: "solid 1px black",
        }}
      >
        {date.format("MM") + "月" + date.format("DD") + "日"}
      </div>
      <section
        style={{
          background: "#E1FFDA",
          padding: "14px 8px",
          border: "solid 1px black",
          height: "76px",
        }}
      >
        <div>{amSchedule.doctorName}</div>
        <div style={{ color: "rgb(128,128,0)" }}>{amSchedule.shiftTime}</div>
      </section>
      <section
        style={{
          background: "#FFCCFF",
          padding: "14px 8px",
          border: "solid 1px black",
          height: "76px",
        }}
      >
        <div>{pmSchedule.doctorName}</div>
        <div style={{ color: "rgb(128,128,0)" }}>{pmSchedule.shiftTime}</div>
      </section>
      <section
        style={{
          background: "#CCCCFF",
          padding: "14px 8px",
          border: "solid 1px black",
          height: "76px",
        }}
      >
        <div>{eveningSchedule.doctorName}</div>
        <div style={{ color: "rgb(128,128,0)" }}>
          {eveningSchedule.shiftTime}
        </div>
      </section>
    </Box>
  );
}
