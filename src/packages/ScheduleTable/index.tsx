import { Box } from "@mui/material";
import dayjs from "dayjs";
import { useMemo } from "react";
import { Schedule, ShiftId } from "../../types/schedule";
import DaySchedule, { DayScheduleHeader } from "../DaySchedule";

interface Props {
  schedules: Schedule[];
}

const SHIFT_TIME: Record<ShiftId, string> = {
  AM: "0830~1200",
  PM: "1400~1700",
  EVENING: "1800~2100",
};

const EMPTY_SHIFT_DATA = {
  refShiftName: "",
  doctorName: "",
  shiftTime: "",
  schDate: "",
  hixClinicRoomName: "",
};

export default function ScheduleTable({ schedules }: Props) {
  const scheduleDayMap = useMemo(() => {
    const dayMap = {
      Mon: {
        AM: { ...EMPTY_SHIFT_DATA },
        PM: { ...EMPTY_SHIFT_DATA },
        EVENING: { ...EMPTY_SHIFT_DATA },
      },
      Tue: {
        AM: { ...EMPTY_SHIFT_DATA },
        PM: { ...EMPTY_SHIFT_DATA },
        EVENING: { ...EMPTY_SHIFT_DATA },
      },
      Wed: {
        AM: { ...EMPTY_SHIFT_DATA },
        PM: { ...EMPTY_SHIFT_DATA },
        EVENING: { ...EMPTY_SHIFT_DATA },
      },
      Thu: {
        AM: { ...EMPTY_SHIFT_DATA },
        PM: { ...EMPTY_SHIFT_DATA },
        EVENING: { ...EMPTY_SHIFT_DATA },
      },
      Fri: {
        AM: { ...EMPTY_SHIFT_DATA },
        PM: { ...EMPTY_SHIFT_DATA },
        EVENING: { ...EMPTY_SHIFT_DATA },
      },
      Sat: {
        AM: { ...EMPTY_SHIFT_DATA },
        PM: { ...EMPTY_SHIFT_DATA },
        EVENING: { ...EMPTY_SHIFT_DATA },
      },
    };

    for (let s of schedules) {
      const {
        refShiftId,
        refShiftName,
        doctorName,
        schDate,
        hixClinicRoomName,
      } = s;
      const day = dayjs(schDate).format("ddd") as keyof typeof dayMap;

      dayMap[day][refShiftId] = {
        refShiftName,
        doctorName,
        shiftTime: SHIFT_TIME[refShiftId],
        schDate,
        hixClinicRoomName,
      };
    }

    return dayMap;
  }, [schedules]);

  // 依照Mon to Sat排
  return (
    <Box
      sx={{
        display: "flex",
      }}
    >
      <DayScheduleHeader
        amRoomName={scheduleDayMap.Mon.AM.hixClinicRoomName}
        pmRoomName={scheduleDayMap.Mon.PM.hixClinicRoomName}
        eveningRoomName={scheduleDayMap.Mon.EVENING.hixClinicRoomName}
      />
      <DaySchedule
        day="星期一"
        date={dayjs(scheduleDayMap.Mon.AM.schDate)}
        amSchedule={scheduleDayMap.Mon.AM}
        pmSchedule={scheduleDayMap.Mon.PM}
        eveningSchedule={scheduleDayMap.Mon.EVENING}
      />
      <DaySchedule
        day="星期二"
        date={dayjs(scheduleDayMap.Tue.AM.schDate)}
        amSchedule={scheduleDayMap.Tue.AM}
        pmSchedule={scheduleDayMap.Tue.PM}
        eveningSchedule={scheduleDayMap.Tue.EVENING}
      />
      <DaySchedule
        day="星期三"
        date={dayjs(scheduleDayMap.Wed.AM.schDate)}
        amSchedule={scheduleDayMap.Wed.AM}
        pmSchedule={scheduleDayMap.Wed.PM}
        eveningSchedule={scheduleDayMap.Wed.EVENING}
      />
      <DaySchedule
        day="星期四"
        date={dayjs(scheduleDayMap.Thu.AM.schDate)}
        amSchedule={scheduleDayMap.Thu.AM}
        pmSchedule={scheduleDayMap.Thu.PM}
        eveningSchedule={scheduleDayMap.Thu.EVENING}
      />
      <DaySchedule
        day="星期五"
        date={dayjs(scheduleDayMap.Fri.AM.schDate)}
        amSchedule={scheduleDayMap.Fri.AM}
        pmSchedule={scheduleDayMap.Fri.PM}
        eveningSchedule={scheduleDayMap.Fri.EVENING}
      />
      <DaySchedule
        day="星期六"
        date={dayjs(scheduleDayMap.Sat.AM.schDate)}
        amSchedule={scheduleDayMap.Sat.AM}
        pmSchedule={scheduleDayMap.Sat.PM}
        eveningSchedule={scheduleDayMap.Sat.EVENING}
      />
    </Box>
  );
}
