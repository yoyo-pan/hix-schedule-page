import { Box } from "@mui/system";
import dayjs, { Dayjs } from "dayjs";
import { ViewInfo } from "../../types/viewInfo";

const amStart = dayjs().hour(8).minute(30);
const amEnd = dayjs().hour(12).minute(0);
const pmStart = dayjs().hour(14).minute(0);
const pmEnd = dayjs().hour(17).minute(0);
const eveningStart = dayjs().hour(18).minute(0);
const eveningEnd = dayjs().hour(21).minute(0);

interface Props {
  viewInfo: ViewInfo[];
}

function isInvalidTime(time: Dayjs) {
  return (
    time.isBefore(amStart) ||
    (time.isAfter(amEnd) && time.isBefore(pmStart)) ||
    (time.isAfter(pmEnd) && time.isBefore(eveningStart)) ||
    time.isAfter(eveningEnd)
  );
}

export default function CurrentViewNumber(props: Props) {
  if (props.viewInfo.length === 0) {
    return null;
  }

  // 一診或二診其中一個有機會為空
  const confirmedViewInfo = props.viewInfo[0].viewInfo
    ? props.viewInfo[0]
    : props.viewInfo[1];
  if (!confirmedViewInfo.viewInfo || !confirmedViewInfo.viewInfo.dateTime) {
    return null;
  }

  const viewInfoTime = dayjs(confirmedViewInfo.viewInfo.dateTime);

  if (isInvalidTime(viewInfoTime)) {
    return null;
  }

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        mb: "12px",
      }}
    >
      <div style={{ background: "#F0F0F0" }}>目前看診號碼</div>
      <Box sx={{ display: "flex", mt: "2px" }}>
        <div
          style={{
            width: "16px",
            background: "#F0F0F0",
            border: "solid 1px orange",
          }}
        >
          {confirmedViewInfo.name}
        </div>
        <div
          style={{
            fontSize: "52px",
            lineHeight: "52px",
            fontWeight: "bold",
            color: "red",
            background: "#000000",
            border: "solid 1px orange",
          }}
        >
          {confirmedViewInfo.viewInfo.no?.toString().padStart(3, "0")}
        </div>
      </Box>
    </Box>
  );
}
